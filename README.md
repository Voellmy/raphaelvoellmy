# Resume Web Site

This repo contains all code of my resume website at https://raphaelvoellmy.ch. The website is built with Gatsby and Prismic headless CMS, deployed with Netlifly.

[![Netlify Status](https://api.netlify.com/api/v1/badges/3c8df869-8b9d-4545-979f-6ca838c0f7bd/deploy-status)](https://app.netlify.com/sites/raphaelvoellmy/deploys)

Features:

* Offline capable (can be added to the homescreen)
* Google Analytics built in
* Easily auto deployed when content is changed in Prismic.io



## Configuration

### Theme 

Some base colors can be customized using the `theme.json` file in the root of this project.

### Favicon

Place any image under `src/assets/favicon.png` and the favicons will be generated automatically.

### Personalize

The name and short name is set in the `config.json`. Make sure the short name is equal or less than 12 characters.

### Prismic

The custom Prismic types are saved in the `config/homepage.json` file.

### Google Analytics

To enable google analytics, all you need to do is define the `GA_TRACKING_ID` when the project is built.