require('dotenv').config({
  path: `.env`,
})

const { name, shortName } = require('./config.json')
const theme = require('./theme.json')

const plugins = [
  'gatsby-plugin-react-helmet',
  'gatsby-plugin-emotion',
  {
    resolve: `gatsby-plugin-netlify`,
    options: {},
  },
  {
    resolve: 'gatsby-source-prismic',
    options: {
      repositoryName: 'raphaelvoellmy',
      accessToken: process.env.API_KEY,
    },
  },
  {
    resolve: `gatsby-plugin-manifest`,
    options: {
      icon: `src/assets/favicon.png`,
      name,
      short_name: shortName,
      start_url: `/`,
      background_color: theme.bgColor,
      theme_color: theme.primaryColor,
      display: `standalone`,
    },
  },
  'gatsby-plugin-offline',
]

// Google Analytics
if (process.env.GA_TRACKING_ID) {
  plugins.push({
    resolve: `gatsby-plugin-gtag`,
    options: {
      // your google analytics tracking id
      trackingId: process.env.GA_TRACKING_ID,
      // Puts tracking script in the head instead of the body
      head: false,
      // enable ip anonymization
      anonymize: true,
    },
  })
}

module.exports = {
  plugins,
}
