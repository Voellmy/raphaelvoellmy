const puppeteer = require('puppeteer')

const { name } = require('./config.json')

;(async () => {
  const browser = await puppeteer.launch()
  const page = await browser.newPage()
  await page.goto(`file://${__dirname}/public/index.html`)
  await page.emulateMedia('print')
  await page.pdf({
    path: `public/${name}.pdf`,
    format: 'A4',
    margin: {
      left: '1.5cm',
      right: '1.5cm',
      top: '2cm',
      bottom: '2cm',
    },
  })
  await browser.close()
})()
